import sys, csv
# CSV data
# rowid, evType, message, n, s, e, w

# eventual (undecided)
# rowid, evType, message, n, s, e, w, objects
class Adventure(object):
        def __init__(self, row):
                self.data = {}
                self.row = row

        def load(self, adv):
                game = csv.reader(open(adv, 'r'))
                for row in game:
                        self.data[eval(row[0])] = row[1:]
                del game

        def state(self):
                tmp = eval(self.data[self.row][0])
                if tmp == 0:
                        return
                elif tmp == -1 or tmp == 1:
                        input()
                        sys.exit(1)
                else:
                        print("Someone made an oopsie. Go pester them to set"\
                              " things straight.")
                        input()
                        sys.exit(-1)

        def message(self):
                print(self.data[self.row][1])

        def move(self, direction):
                tmp = self.data[self.row][direction]
                try:
                        self.row = eval(tmp)
                except:
                        print(tmp)
                        self.command()

        def command(self):
                userInput = input(">>> ")

                try:
                        if eval(type(userInput)) != str:
                                self.usage()
                                self.command()
                except:
                        pass

                userInput = userInput.lower()
                if userInput == 'n':
                        self.move(2)
                elif userInput == 's':
                        self.move(3)
                elif userInput == 'e':
                        self.move(4)
                elif userInput == 'w':
                        self.move(5)
                else:
                        self.usage()
                        self.command()

        def usage(self):
                print("I do not understand that command")
                print("  n or N for north")
                print("  s or S for south")
                print("  e or E for east")
                print("  w or W for west")

        def row(self, row):
                return self.data[row]

def main(adv):
        game = Adventure(0)
        game.load(adv)
        while True:
                game.message()
                game.state()
                game.command()

if __name__ == "__main__":
        main(sys.argv[1])
